## Step 1: Critical reading of literature & identifying arguments

When we start out writing a research proposal, the first step is to identify relevant literature on the topic and after identifying relevant literature, we will need to closely read the literature to identify what's missing in the literature. At the same time, such reading of the literature will also help us to identify the key themes in the knowledge base and based on these themes and our questions, we can narrow our research questions further. 

Because we need to identify the key themes in the knowledge base first, we need to have a plan of robust search and retrieval of key literature on the topic. A good place to start is to identify a review on the topic. The review can be a systematic review, a narrative review, or even a meta-analysis that can provide us links to key primary studies and assimilation of knowledge of the secondary sources. Another place to look for gaining insight into the nature of the problem are data maintained on this topic at various statistical databases. We can download data from those databases and we can analyse the raw data to gain insights into the nature of the problem. In New Zealand, Stats New Zealand and the Ministry of Health of New Zealand distribute health data free of charge to everyone, and these are great places to start investigating a research problem. Besides this source, you can access data from numerous sources, and web based databases including kaggle, CDC, WHO, and other sources. Analyses of the raw data provide you with insights that you can use to develop graphs and charts yourself that will let you fine tune your query. 

In this section, therefore, you need to master two key skills:

1. You must master how to access and analyse data from administrative databases, or other sources.
2. You must master the art of critical reading and querying an article or a source of information

Let's take a look at each of these in turn.